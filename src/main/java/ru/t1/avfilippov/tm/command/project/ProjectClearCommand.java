package ru.t1.avfilippov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "remove all projects";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

}
