package ru.t1.avfilippov.tm.command.task;

import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "create new task";
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

}
